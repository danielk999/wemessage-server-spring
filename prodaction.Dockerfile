#### STAGE 1: Build ###
#FROM maven:3.8.3-jdk-11-slim AS build
#WORKDIR /workspace
#COPY pom.xml ./
#COPY src ./src
#RUN mvn -f pom.xml clean package
#
### STAGE 2: Run ###
FROM adoptopenjdk/openjdk11-openj9
WORKDIR /workspace
COPY target/*.jar app.jar
ENTRYPOINT ["java","-jar","app.jar"]

##### STAGE 1: Build ###
#FROM maven:3.8.3-jdk-11-slim AS build
#WORKDIR /workspace
#COPY pom.xml ./
#COPY src ./src
#COPY mvnw ./
#COPY mvnw.cmd ./
#RUN mvn dependecy:resolve
#RUN mvn dependency:resolve-plugins
#
#
##### STAGE 2: Run ###
#FROM maven:3.8.3-jdk-11-slim
#WORKDIR /workspace
#COPY --from=build /workspace ./
#EXPOSE 8080
#ENTRYPOINT ["mvn" , "spring-boot:run"]