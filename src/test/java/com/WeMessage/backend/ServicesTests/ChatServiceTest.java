package com.WeMessage.backend.ServicesTests;

import com.WeMessage.backend.DTOs.AddChatRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.Message;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.NotEnoughUsersException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.repositories.ChatRepository;
import com.WeMessage.backend.repositories.MessageRepository;
import com.WeMessage.backend.repositories.UserRepository;
import com.WeMessage.backend.services.ChatService;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ChatServiceTest {

    @Autowired
    @InjectMocks
    private ChatService service;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ChatRepository chatRepository;


    @Test
    void addChat(){

        User user1 = new User("danielk999", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        User user2 = new User("katzu", DigestUtils.sha256Hex("hhhf"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        List<User> users = new LinkedList<>();
        users.add(user1);
        users.add(user2);
        Chat c = new Chat(1L,"chat1" , users , null);

        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        userNames.add("katzu");
        AddChatRequest chat = new AddChatRequest("chat1" , userNames);

        when(userRepository.findByUserName("danielk999")).thenReturn(user1);
        when(userRepository.findByUserName("katzu")).thenReturn(user2);
        when(chatRepository.save(any())).thenReturn(c);
        Assertions.assertDoesNotThrow(()->{
            boolean added = service.addChat(chat);
            assert (added);
        });
    }

    @Test
    void addChatMissingUsers(){
        User user1 = new User("danielk999", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        List<User> users = new LinkedList<>();
        users.add(user1);
        Chat c = new Chat(1L,"chat1" , users , null);

        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        AddChatRequest chat = new AddChatRequest("chat1" , userNames);

        Assertions.assertThrows(NotEnoughUsersException.class , ()->{
            service.addChat(chat);
        });
    }

    @Test
    void addChatWrongUser(){
        User user1 = new User("danielk999", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        User user2 = new User("katzu", DigestUtils.sha256Hex("hhhf"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        List<User> users = new LinkedList<>();
        users.add(user1);
        users.add(user2);
        Chat c = new Chat(1L,"chat1" , users , null);

        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        userNames.add("katzu");
        AddChatRequest chat = new AddChatRequest("chat1" , userNames);

        when(userRepository.findByUserName(anyString())).thenReturn(null);
        Assertions.assertThrows(UserDoesNotExistsException.class , ()->{
            service.addChat(chat);
        });
    }

    @Test
    void getMessages(){

        User user = new User("katzu","hhhh","daniel" ,"katz","kk@gmail.com" , new Date(), null);
        Chat chat = new Chat(2L,"chat1" , null , null);
        Message m1 = new Message(3L,"24-07-1999" , "hello1" , user , chat);
        Message m2 = new Message(4L,"24-07-1999" , "hello2" , user , chat);
        List<Message> messages = new LinkedList<>();
        messages.add(m1);
        messages.add(m2);
        chat.setChatMessages(messages);

        when(chatRepository.findByChatId(anyLong())).thenReturn(chat);

        Assertions.assertDoesNotThrow(()->{
            List<MessageDTO> chatMessages = service.getMessages(2L);
            assert (chatMessages.size() == 2);
            assert (chatMessages.get(0).getId() == 3);
            assert (chatMessages.get(0).getBody().equals("hello1"));
            assert (chatMessages.get(1).getId() == 4);
            assert (chatMessages.get(1).getBody().equals("hello2"));
        });
    }

    @Test
    void getMessagesNoChat(){

        when(chatRepository.findByChatId(anyLong())).thenReturn(null);

        Assertions.assertThrows(ChatDoesNotExistsException.class , ()->{
           service.getMessages(3L);
        });
    }
}
