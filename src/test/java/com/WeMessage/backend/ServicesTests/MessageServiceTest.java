package com.WeMessage.backend.ServicesTests;

import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.Message;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.repositories.ChatRepository;
import com.WeMessage.backend.repositories.MessageRepository;
import com.WeMessage.backend.repositories.UserRepository;
import com.WeMessage.backend.services.MessageService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class MessageServiceTest {

    @Autowired
    @InjectMocks
    private MessageService service;

    @Mock
    private MessageRepository messageRepository;

    @Mock
    private UserRepository userRepository;

    @Mock
    private ChatRepository chatRepository;

    @Test
    void addMessageNoUser (){
        when(userRepository.findByUserName(anyString())).thenReturn(null);
        Assertions.assertThrows(UserDoesNotExistsException.class , ()->{
           service.addMessage(new MessageDTO(3L,"24-04-1999" , "hello" , "katzu" , 1L));
        });
    }

    @Test
    void addMessageNoChat (){
        User user = new User("katzu","hhhh","daniel" ,"katz","kk@gmail.com" , new Date(), null);
        when(userRepository.findByUserName(anyString())).thenReturn(user);
        when(chatRepository.findByChatId(anyLong())).thenReturn(null);
        Assertions.assertThrows(ChatDoesNotExistsException.class , ()->{
           service.addMessage(new MessageDTO(3L,"24-04-1999" , "hello" , "katzu" , 1L));
        });
    }

    @Test
    void addMessage (){
        User user = new User("katzu","hhhh","daniel" ,"katz","kk@gmail.com" , new Date(), null);
        Chat chat = new Chat(2L,"chat1" , null , null);
        Message m = new Message(3L,"24-07-1999" , "hello" , user , chat);
        when(userRepository.findByUserName(anyString())).thenReturn(user);
        when(chatRepository.findByChatId(anyLong())).thenReturn(chat);
        when(messageRepository.save(any())).thenReturn(m);
        Assertions.assertDoesNotThrow(()->{
            Long messageId = service.addMessage(new MessageDTO(3L , "24-07-1999" , "hello" , "katzu" , 2L));
            assert (messageId == 3);
        });
    }
}
