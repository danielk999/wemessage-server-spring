package com.WeMessage.backend.ServicesTests;

import com.WeMessage.backend.DTOs.ChatDTO;
import com.WeMessage.backend.DTOs.UserDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserExistsException;
import com.WeMessage.backend.exceptions.WrongPasswordException;
import com.WeMessage.backend.mappers.UserMapper;
import com.WeMessage.backend.repositories.UserRepository;
import com.WeMessage.backend.services.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {

    @Autowired
    @InjectMocks
    private UserService service;

    @Mock
    private UserRepository repository;

    @Test
    void registerExistingUser() {
        User user = new User("danielk999", "hhhh", "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        when(repository.findByUserName(anyString())).thenReturn(user);
        Assertions.assertThrows(UserExistsException.class, () -> {
            service.singUp(UserMapper.toUserDTO(user), user.getPasswordHash());
        });
    }

    @Test
    void registerNoUserName() {
        UserDTO user = new UserDTO("", "daniel", "katz", "hello@gmail.com", "24-07-1999");
        Assertions.assertThrows(BadInputException.class, () -> {
            service.singUp(user, "hhhh");
        });

    }

    @Test
    void registerNoUserPass() {
        UserDTO user = new UserDTO("danielk999", "daniel", "katz", "hello@gmail.com", "24-07-1999");
        Assertions.assertThrows(BadInputException.class, () -> {
            service.singUp(user, "");
        });
    }


    @Test
    void registerNewUser() {
        Date date = null;
        try {
            date = new SimpleDateFormat("dd-mm-yyyy").parse("24-07-1999");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        User user = new User("danielk999", "hhhh", "daniel", "katz", "hello@gmail.com", date , new LinkedList<>());
        when(repository.save(any())).thenReturn(user);
        UserDTO returnUser = null;
        try {
            returnUser = service.singUp(UserMapper.toUserDTO(user), "hhhh");
        } catch (Exception e) {
            assert (false);
        }

        assert (returnUser.getUserName().equals("danielk999"));
        assert (returnUser.getFirstName().equals("daniel"));
        assert (returnUser.getLastName().equals("katz"));
        assert (returnUser.getEmail().equals("hello@gmail.com"));
        assert (returnUser.getDob().equals("24-07-1999"));
    }

    @Test
    void logInExistingUser() {
        Date date = null;
        try {
            date = new SimpleDateFormat("dd-mm-yyyy").parse("24-07-1999");
        } catch (ParseException e) {
            e.printStackTrace();
        }
        User user = new User("danielk999", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", date , new LinkedList<>());
        when(repository.getById("danielk999")).thenReturn(user);

        UserDTO returnUser = null;
        try {
            returnUser = service.logIn("danielk999", "hhhh");
        } catch (Exception e) {
            assert (false);
        }
        assert (returnUser.getUserName().equals("danielk999"));
        assert (returnUser.getFirstName().equals("daniel"));
        assert (returnUser.getLastName().equals("katz"));
        assert (returnUser.getEmail().equals("hello@gmail.com"));
        assert (returnUser.getDob().equals("24-07-1999"));
    }

    @Test
    void LogInNoUserName() {
        boolean thrown = false;
        Assertions.assertThrows(BadInputException.class, () -> {
            service.logIn("", "hhhh");
        });
    }

    @Test
    void LogInNoUserPass() {
        boolean thrown = false;
        Assertions.assertThrows(BadInputException.class, () -> {
            service.logIn("daniel", "");
        });
    }

    @Test
    void logInNotExistingUser() {
        when(repository.getById(anyString())).thenThrow();
        Assertions.assertThrows(UserDoesNotExistsException.class, () -> {
            service.logIn("daniel", "ddd");
        });
    }

    @Test
    void logInWrongPass() {
        User user = new User("danielk999", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        when(repository.getById(anyString())).thenReturn(user);
        Assertions.assertThrows(WrongPasswordException.class, () -> {
            service.logIn(user.getUserName(), "ddd");
        });
    }

    @Test
    void getUsersByPartialName() {
        User user1 = new User("d", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()),new LinkedList<>());
        User user2 = new User("p", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()) , new LinkedList<>());
        Collection<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        when(repository.findByPartialUserName(anyString())).thenReturn(users);
        Collection<UserDTO> returnUsers = service.getUsersContainsName("");
        assert(((UserDTO)returnUsers.toArray()[0]).getUserName().equals("d"));
        assert(((UserDTO)returnUsers.toArray()[1]).getUserName().equals("p"));
    }

    @Test
    void getChatsExistingUser () {
        Chat chat1 = new Chat(1L,"chat1" , new LinkedList<>() , new LinkedList<>());
        Chat chat2 = new Chat(2L,"chat2" , new LinkedList<>() , new LinkedList<>());
        List<Chat> chats = new LinkedList<>();
        chats.add(chat1);
        chats.add(chat2);
        User user = new User("d", DigestUtils.sha256Hex("hhhh"), "daniel", "katz", "hello@gmail.com", new Date(System.currentTimeMillis()),chats);
        when(repository.findByUserName(anyString())).thenReturn(user);
        Assertions.assertDoesNotThrow(() ->{
            List<ChatDTO> userChats = service.getChats("d");
            assert(((ChatDTO)userChats.toArray()[0]).getId() == 1);
            assert(((ChatDTO)userChats.toArray()[1]).getId() == 2);
            assert(((ChatDTO)userChats.toArray()[0]).getChatName().equals("chat1"));
            assert(((ChatDTO)userChats.toArray()[1]).getChatName().equals("chat2"));
        });
    }

    @Test
    void getChatsNotExistingUser () {
        when(repository.findByUserName(anyString())).thenThrow();
        Assertions.assertThrows(UserDoesNotExistsException.class, () -> {
            List<ChatDTO> userChats = service.getChats("d");
        });
    }
}
