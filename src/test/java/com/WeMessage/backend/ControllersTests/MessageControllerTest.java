package com.WeMessage.backend.ControllersTests;

import com.WeMessage.backend.DTOs.AddMessageRequest;
import com.WeMessage.backend.controllers.MessageController;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.services.MessageService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(MessageController.class)
public class MessageControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private MessageService messageService;

    @Test
    void addMessage() throws Exception {
        AddMessageRequest messageRequest = new AddMessageRequest("24-02-2021", "hello" , "katzu" , "3");
        ObjectMapper mapper = new ObjectMapper();
        when(messageService.addMessage(any())).thenReturn(1L);
        mockMvc.perform(MockMvcRequestBuilders.post("/messages/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(messageRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));
    }

    @Test
    void addMessageNoUser() throws Exception {
        AddMessageRequest messageRequest = new AddMessageRequest("24-02-2021", "hello" , "katzu" , "3");
        ObjectMapper mapper = new ObjectMapper();
        when(messageService.addMessage(any())).thenThrow(UserDoesNotExistsException.class);
        mockMvc.perform(MockMvcRequestBuilders.post("/messages/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(messageRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("USER_DOESNT_EXIST"));
    }

    @Test
    void addMessageNoChat() throws Exception {
        AddMessageRequest messageRequest = new AddMessageRequest("24-02-2021", "hello" , "katzu" , "3");
        ObjectMapper mapper = new ObjectMapper();
        when(messageService.addMessage(any())).thenThrow(ChatDoesNotExistsException.class);
        mockMvc.perform(MockMvcRequestBuilders.post("/messages/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(messageRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("CHAT_DOESNT_EXIST"));
    }


}
