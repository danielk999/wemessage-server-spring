package com.WeMessage.backend.ControllersTests;

import com.WeMessage.backend.DTOs.AddChatRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.controllers.ChatRestController;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.NotEnoughUsersException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.services.ChatService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ChatRestController.class)
public class ChatControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private ChatService service;

    @Test
    void addChat() throws Exception{

        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        userNames.add("katzu");
        AddChatRequest request = new AddChatRequest("chat1" , userNames);

        when(service.addChat(any())).thenReturn(true);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/chats/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"));


    }

    @Test
    void addChatMissingUsers() throws Exception{
        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        AddChatRequest request = new AddChatRequest("chat1" , userNames);

        when(service.addChat(any())).thenThrow(NotEnoughUsersException.class);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/chats/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("BAD_INPUT"));

    }

    @Test
    void addChatWrongUser() throws Exception{
        List<String> userNames = new LinkedList<>();
        userNames.add("danielk999");
        userNames.add("katzu");
        AddChatRequest request = new AddChatRequest("chat1" , userNames);

        when(service.addChat(any())).thenThrow(UserDoesNotExistsException.class);

        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/chats/add")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("USER_DOESNT_EXIST"));

    }

    @Test
    void getMessages() throws Exception{

        MessageDTO m1 = new MessageDTO(3L,"24-07-1999" , "hello1" ,"katzu", 2L);
        MessageDTO m2 = new MessageDTO(4L,"24-07-1999" , "hello2" , "katzu" , 2L);
        List<MessageDTO> messages = new LinkedList<>();
        messages.add(m1);
        messages.add(m2);

        when(service.getMessages(anyLong())).thenReturn(messages);

        mockMvc.perform(MockMvcRequestBuilders.get("/messages/get?chatId=2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.body[0].id" ,  is(3)))
                .andExpect(jsonPath("$.body[0].body" ).value("hello1"))
                .andExpect(jsonPath("$.body[1].id" ,  is(4)))
                .andExpect(jsonPath("$.body[1].body" ).value("hello2"));

    }

    @Test
    void getMessagesNoChat() throws Exception{

        when(service.getMessages(anyLong())).thenThrow(ChatDoesNotExistsException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/messages/get?chatId=2")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("CHAT_DOESNT_EXIST"));
    }
}
