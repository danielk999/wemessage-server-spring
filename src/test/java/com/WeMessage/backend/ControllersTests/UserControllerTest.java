package com.WeMessage.backend.ControllersTests;

import com.WeMessage.backend.DTOs.ChatDTO;
import com.WeMessage.backend.DTOs.LogInRequest;
import com.WeMessage.backend.DTOs.UserDTO;
import com.WeMessage.backend.DTOs.RegisterRequest;
import com.WeMessage.backend.controllers.UserController;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserExistsException;
import com.WeMessage.backend.services.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    private UserService userService;

    @Test
    void registerNewUser() throws Exception {
        UserDTO userDTO = new UserDTO("danielk999","daniel","katz","hello@gmail.com","24-07-1999");
        when(userService.singUp(any(),anyString())).thenReturn(userDTO);
        RegisterRequest request = new RegisterRequest("danielk999","hello@gmail.com","1234","daniel","katz","24-07-1999");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.status").value("OK"))
        .andExpect(jsonPath("$.body.userName").value("danielk999"))
        .andExpect(jsonPath("$.body.firstName").value("daniel"))
        .andExpect(jsonPath("$.body.lastName").value("katz"))
        .andExpect(jsonPath("$.body.email").value("hello@gmail.com"))
        .andExpect(jsonPath("$.body.dob").value("24-07-1999"));
    }

    @Test
    void registerExitingUser() throws Exception {
        when(userService.singUp(any(),anyString())).thenThrow(UserExistsException.class);
        RegisterRequest request = new RegisterRequest("danielk999","hello@gmail.com","1234","daniel","katz","24-07-1999");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/register")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("USER_EXISTS"));
    }


    @Test
    void logInExitingUser() throws Exception {
        UserDTO userDTO = new UserDTO("danielk999","daniel","katz","hello@gmail.com","24-07-1999");
        when(userService.logIn(anyString(),anyString())).thenReturn(userDTO);
        LogInRequest request = new LogInRequest("danielk999","1234");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.body.userName").value("danielk999"))
                .andExpect(jsonPath("$.body.firstName").value("daniel"))
                .andExpect(jsonPath("$.body.lastName").value("katz"))
                .andExpect(jsonPath("$.body.email").value("hello@gmail.com"))
                .andExpect(jsonPath("$.body.dob").value("24-07-1999"));
    }
    @Test
    void logInNoExitingUser() throws Exception {
        when(userService.logIn(anyString(),anyString())).thenThrow(UserDoesNotExistsException.class);
        LogInRequest request = new LogInRequest("danielk999","1234");
        ObjectMapper mapper = new ObjectMapper();
        mockMvc.perform(MockMvcRequestBuilders.post("/login")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("WRONG_CREDENTIALS"));
    }

    @Test
    void getUsersByPartialName() throws Exception {
        UserDTO user1 = new UserDTO("d","d","d","d@gmail.com","24-02-1999");
        UserDTO user2 = new UserDTO("p","d","d","d@gmail.com","25-02-1999");
        Collection<UserDTO> users = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        users.add(user1);
        users.add(user2);
        when(userService.getUsersContainsName(anyString())).thenReturn(users);
        mockMvc.perform(MockMvcRequestBuilders.get("/users?data=i")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.body").isArray())
                .andExpect(jsonPath("$.body[0].userName", is("d")))
                .andExpect(jsonPath("$.body[1].userName", is("p")));
    }

    @Test
    void getChatsExistingUser () throws Exception{
        ChatDTO chatDTO1 = new ChatDTO(1L , "chat1");
        ChatDTO chatDTO2 = new ChatDTO(2L , "chat2");
        List<ChatDTO> chats = new LinkedList<ChatDTO>();
        chats.add(chatDTO1);
        chats.add(chatDTO2);
        when(userService.getChats("katzu")).thenReturn(chats);
        mockMvc.perform(MockMvcRequestBuilders.get("/chats/get?userName=katzu")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("OK"))
                .andExpect(jsonPath("$.body").isArray())
                .andExpect(jsonPath("$.body[0].id", is(1)))
                .andExpect(jsonPath("$.body[0].chatName", is("chat1")))
                .andExpect(jsonPath("$.body[1].id", is(2)))
                .andExpect(jsonPath("$.body[1].chatName", is("chat2")));
    }

    @Test
    void getChatsNotExistingUser () throws Exception{
        when(userService.getChats("katzu")).thenThrow(UserDoesNotExistsException.class);
        mockMvc.perform(MockMvcRequestBuilders.get("/chats/get?userName=katzu")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status").value("USER_DOESNT_EXIST"));
    }
}
