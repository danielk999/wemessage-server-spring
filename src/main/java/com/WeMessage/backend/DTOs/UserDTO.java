package com.WeMessage.backend.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class UserDTO {

    private String userName;

    private String firstName;

    private String lastName;

    private String email;

    //date as a string
    private String dob;

}
