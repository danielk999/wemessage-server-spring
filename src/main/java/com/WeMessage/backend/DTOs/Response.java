package com.WeMessage.backend.DTOs;


import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class Response<T> {
    public enum Status{
        OK,
        WRONG_CREDENTIALS,
        USER_EXISTS,
        BAD_INPUT,
        Error,
        USER_DOESNT_EXIST,
        CHAT_DOESNT_EXIST
    }

    private Status status;
    private T body;

    public static <T> Response<T> ok(){
        Response<T> response = new Response<>();
        response.setStatus(Status.OK);
        return response;
    }

    public static <T> Response<T> badPass(){
        Response<T> response = new Response<>();
        response.setStatus(Status.WRONG_CREDENTIALS);
        return response;
    }

    public static <T> Response<T> badUserName(){
        Response<T> response = new Response<>();
        response.setStatus(Status.USER_EXISTS);
        return response;
    }

    public static <T> Response<T> badInput(){
        Response<T> response = new Response<>();
        response.setStatus(Status.BAD_INPUT);
        return response;
    }
    public static <T> Response<T> error(){
        Response<T> response = new Response<>();
        response.setStatus(Status.Error);
        return response;
    }
    public static <T> Response<T> noUser(){
        Response<T> response = new Response<>();
        response.setStatus(Status.USER_DOESNT_EXIST);
        return response;
    }
    public static <T> Response<T> noChat(){
        Response<T> response = new Response<>();
        response.setStatus(Status.CHAT_DOESNT_EXIST);
        return response;
    }
}
