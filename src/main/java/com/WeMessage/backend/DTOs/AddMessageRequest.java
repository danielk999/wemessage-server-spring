package com.WeMessage.backend.DTOs;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class AddMessageRequest {
    private String date;

    private String body;

    private String senderUserName;

    private String chatId;
}
