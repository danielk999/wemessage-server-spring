package com.WeMessage.backend.DTOs;

import lombok.*;

import java.util.List;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class AddChatRequest {

    private String chatName;

    private List<String> userNames;
}
