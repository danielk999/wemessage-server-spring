package com.WeMessage.backend.repositories;

import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository extends JpaRepository<User,String> {

    User findByUserName(String userName);

    @Query("SELECT u FROM User u WHERE u.userName LIKE CONCAT('%',:username,'%')")
    Collection<User> findByPartialUserName(@Param("username")String partialName);

}
