package com.WeMessage.backend.repositories;

import com.WeMessage.backend.entities.Message;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageRepository extends JpaRepository<Message, Long> {
}
