package com.WeMessage.backend.repositories;

import com.WeMessage.backend.entities.Chat;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ChatRepository extends JpaRepository<Chat,Long> {

    Chat findByChatId(Long chatId);
}
