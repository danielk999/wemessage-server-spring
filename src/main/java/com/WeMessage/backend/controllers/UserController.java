package com.WeMessage.backend.controllers;

import com.WeMessage.backend.DTOs.*;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserExistsException;
import com.WeMessage.backend.exceptions.WrongPasswordException;
import com.WeMessage.backend.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.List;

import static com.WeMessage.backend.mappers.UserMapper.map;

@RestController
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;


    @PostMapping("/register")
    public @ResponseBody Response register(@RequestBody RegisterRequest request){
        Response<UserDTO> response= Response.ok();
        try {
            response.setBody(userService.singUp(map(request),request.getPassword()));
            return response;
        }catch (BadInputException e){
            return Response.badInput();
        }catch (UserExistsException e){
            return Response.badUserName();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Response.error();
        }
    }

    @PostMapping("/login")
    public @ResponseBody Response login(@RequestBody LogInRequest request){
        Response<UserDTO> response= Response.ok();
        try {
            response.setBody(userService.logIn(request.getUserName(),request.getPassword()));
            return response;
        }catch (BadInputException e){
            return Response.badInput();
        }catch (UserDoesNotExistsException e){
            return Response.badPass();
        }catch (WrongPasswordException e){
            return Response.badPass();
        }catch (Exception e){
            System.out.println(e.getMessage());
            return Response.error();
        }
    }

    @GetMapping(value="/users")
    public @ResponseBody Response getUsers(@RequestParam("data") String partialName){
        Response<Collection<UserDTO>> response= Response.ok();
        response.setBody(userService.getUsersContainsName(partialName));
        return response;
    }

    @GetMapping("/chats/get")
    public @ResponseBody Response getChats(@RequestParam("userName") String userName){
        try{
            List<ChatDTO> chats = userService.getChats(userName);
            Response response = Response.ok();
            response.setBody(chats);
            return response;
        }catch (UserDoesNotExistsException existsException){
            return Response.noUser();
        }catch (Exception e){
            return Response.error();
        }
    }


    @GetMapping("/hello")
    public String hello(){
        return "hello world";
    }

}
