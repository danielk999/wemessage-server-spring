package com.WeMessage.backend.controllers;

import com.WeMessage.backend.DTOs.AddChatRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.DTOs.Response;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.NotEnoughUsersException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.services.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
public class ChatRestController {

    @Autowired
    private ChatService chatService;

    @PostMapping("/chats/add")
    public @ResponseBody Response addChat(@RequestBody AddChatRequest request){
        try{
            chatService.addChat(request);
            return Response.ok();
        }catch (BadInputException e){
            return Response.badInput();
        }catch (NotEnoughUsersException e){
            return Response.badInput();
        }catch (UserDoesNotExistsException e){
            return Response.noUser();
        }catch (Exception e){
            return Response.error();
        }
    }

    @GetMapping("/messages/get")
    public @ResponseBody Response getMessages(@RequestParam("chatId") String chatId){
        try {
            Long id = Long.parseLong(chatId);
            List<MessageDTO> messages = chatService.getMessages(id);
            Response response = Response.ok();
            response.setBody(messages);
            return response;
        }catch (NumberFormatException e){
            return Response.badInput();
        }catch (ChatDoesNotExistsException e){
            return Response.noChat();
        }catch (Exception e){
            return Response.error();
        }
    }
	
}
