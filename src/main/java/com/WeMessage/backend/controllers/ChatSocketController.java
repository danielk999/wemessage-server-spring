package com.WeMessage.backend.controllers;

import com.WeMessage.backend.DTOs.AddMessageRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.DTOs.Response;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.mappers.MessageMapper;
import com.WeMessage.backend.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;

@Controller
@CrossOrigin
public class ChatSocketController {

    @Autowired
    private SimpMessagingTemplate messagingTemplate;

    @Autowired
    private MessageService messageService;

    @MessageMapping("/room/{roomId}")
    public void newMessage(@DestinationVariable String roomId ,@Payload AddMessageRequest message) {
        String destination = "/topic/room/"+roomId;
        try {
            MessageDTO newMessage = MessageMapper.requestToDTO(message);
            Long id = messageService.addMessage(newMessage);
            newMessage.setId(id);
            Response response = Response.ok();
            response.setBody(newMessage);
            messagingTemplate.convertAndSend(destination , response);

        }catch (BadInputException e){
            messagingTemplate.convertAndSend(destination , Response.badInput());
        }catch (NumberFormatException e){
            messagingTemplate.convertAndSend(destination , Response.badInput());
        }catch (UserDoesNotExistsException e){
            messagingTemplate.convertAndSend(destination , Response.noUser());
        }catch (ChatDoesNotExistsException e){
            messagingTemplate.convertAndSend(destination , Response.noChat());
        }catch (Exception e){
            messagingTemplate.convertAndSend(destination, Response.error());
        }

    }
}
