package com.WeMessage.backend.controllers;

import com.WeMessage.backend.DTOs.AddMessageRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.DTOs.Response;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.mappers.MessageMapper;
import com.WeMessage.backend.services.MessageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@CrossOrigin
public class MessageController {

    @Autowired
    private MessageService messageService;

    @PostMapping("/messages/add")
    public @ResponseBody Response addMessage(@RequestBody AddMessageRequest addMessageRequest){
        try {
            MessageDTO newMessage = MessageMapper.requestToDTO(addMessageRequest);
            messageService.addMessage(newMessage);
            Response response = Response.ok();
            response.setBody(newMessage);
            return response;
        }catch (BadInputException e){
            return Response.badInput();
        }catch (NumberFormatException e){
            return Response.badInput();
        }catch (UserDoesNotExistsException e){
            return Response.noUser();
        }catch (ChatDoesNotExistsException e){
            return Response.noChat();
        }catch (Exception e){
            return Response.error();
        }
    }


}
