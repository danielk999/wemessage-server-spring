package com.WeMessage.backend.entities;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="USERS")
public class User {
    @Id
    private String userName;

    private String passwordHash;

    private String firstName;

    private String lastName;

    private String email;

    private Date dob;

    @ManyToMany(mappedBy = "users")
    private List<Chat> chats;
}
