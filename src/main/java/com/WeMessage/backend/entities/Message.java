package com.WeMessage.backend.entities;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "MESSAGES")
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String date;

    private String body;

    @ManyToOne
    @JoinColumn(name="userName", nullable=false)
    private User sender;

    @ManyToOne
    @JoinColumn(name="chatId", nullable=false)
    private Chat chat;
}
