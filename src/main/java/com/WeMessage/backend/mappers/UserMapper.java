package com.WeMessage.backend.mappers;

import com.WeMessage.backend.DTOs.RegisterRequest;
import com.WeMessage.backend.DTOs.UserDTO;
import com.WeMessage.backend.entities.User;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;

public class UserMapper {
    public static UserDTO toUserDTO(User usr){
        DateFormat dateFormat = new SimpleDateFormat("dd-mm-yyyy");
        return new UserDTO(usr.getUserName(), usr.getFirstName(), usr.getLastName(),usr.getEmail(),dateFormat.format(usr.getDob()));
    }
    public static User toUser(UserDTO userDTO, String password){
        Date date = null;
        try {
            date =new SimpleDateFormat("dd-mm-yyyy").parse(userDTO.getDob());
        } catch (ParseException e) {
            return null;
        }
        return new User(userDTO.getUserName(),password,userDTO.getFirstName(),userDTO.getLastName(),userDTO.getEmail(),date, new LinkedList<>());
    }

    public static UserDTO map(RegisterRequest request){
        return new UserDTO(request.getUserName(),request.getFirstName(),request.getLastName(),request.getEmail(),request.getDob());
    }

    public static RegisterRequest map(UserDTO request,String password){
        return new RegisterRequest(request.getUserName(),request.getEmail(),password,request.getFirstName(),request.getLastName(),request.getDob());
    }
}
