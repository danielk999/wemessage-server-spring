package com.WeMessage.backend.mappers;

import com.WeMessage.backend.DTOs.ChatDTO;
import com.WeMessage.backend.entities.Chat;

public class ChatMapper {

    public static ChatDTO chatToChatDTO (Chat chat){
        return new ChatDTO(chat.getChatId() , chat.getChatName());
    }
}
