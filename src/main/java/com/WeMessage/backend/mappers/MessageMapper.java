package com.WeMessage.backend.mappers;

import com.WeMessage.backend.DTOs.AddMessageRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.entities.Message;

public class MessageMapper {

    public static MessageDTO messageMapToMessageDTO(Message message){
        return new MessageDTO(message.getId() , message.getDate() ,
                                                        message.getBody() , message.getSender().getUserName() ,
                                                        message.getChat().getChatId());
    }

    public static MessageDTO requestToDTO(AddMessageRequest request) throws NumberFormatException{
        Long chatId = Long.parseLong(request.getChatId());
        return new MessageDTO(null, request.getDate(),
                request.getBody(), request.getSenderUserName(),
                chatId);

    }
}
