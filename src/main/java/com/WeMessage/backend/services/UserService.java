package com.WeMessage.backend.services;

import com.WeMessage.backend.DTOs.ChatDTO;
import com.WeMessage.backend.DTOs.UserDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserExistsException;
import com.WeMessage.backend.exceptions.WrongPasswordException;
import com.WeMessage.backend.mappers.ChatMapper;
import com.WeMessage.backend.mappers.UserMapper;
import com.WeMessage.backend.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public UserDTO singUp(UserDTO userDTO, String password) throws Exception {
        if(userDTO == null ||userDTO.getUserName().equals("")  || password.equals("")){
            throw new BadInputException();
        }
        String sha256Pass = DigestUtils.sha256Hex(password);
        User user = userRepository.findByUserName(userDTO.getUserName());
        if(user == null){
            User userToSave = UserMapper.toUser(userDTO,sha256Pass);
            if(userToSave == null){
                throw new BadInputException();
            }
            User newUser = userRepository.save(userToSave);
            return UserMapper.toUserDTO(newUser);
        }else{
            throw new UserExistsException();
        }
    }

    public UserDTO logIn(String userName, String password) throws Exception {
        if(userName.equals("")  || password.equals("")){
            throw new BadInputException();
        }
        String sha256Pass = DigestUtils.sha256Hex(password);
        try {
            User user = userRepository.getById(userName);
            if (sha256Pass.equals(user.getPasswordHash())) {
                return UserMapper.toUserDTO(user);
            } else {
                throw new WrongPasswordException();
            }
        }catch (WrongPasswordException e){
            throw new WrongPasswordException();
        }catch (Exception e){
            throw new UserDoesNotExistsException();
        }
    }

    public Collection<UserDTO> getUsersContainsName(String partialName){
        Collection<User> dbUsers = userRepository.findByPartialUserName(partialName);
        Collection<UserDTO> mappedUsers = new ArrayList<>();
        for (User u: dbUsers) {
            mappedUsers.add(UserMapper.toUserDTO(u));
        }
        return mappedUsers;
    }

    public List<ChatDTO> getChats(String userName) throws Exception{
        try {
            User user = userRepository.findByUserName(userName);
            if (user == null) {
                throw new UserDoesNotExistsException();
            } else {
                List<ChatDTO> userChats = new LinkedList<>();
                Collection<Chat> chats = user.getChats();
                for (Chat c : chats) {
                    ChatDTO mapChat = ChatMapper.chatToChatDTO(c);
                    userChats.add(mapChat);
                }
                return userChats;
            }
        }catch (Exception e){
            throw new UserDoesNotExistsException();
        }
    }
}
