package com.WeMessage.backend.services;

import com.WeMessage.backend.DTOs.AddChatRequest;
import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.Message;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.NotEnoughUsersException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.mappers.MessageMapper;
import com.WeMessage.backend.repositories.ChatRepository;
import com.WeMessage.backend.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class ChatService {

    @Autowired
    private ChatRepository chatRepository;

    @Autowired
    private UserRepository userRepository;

    public boolean addChat(AddChatRequest request) throws Exception{
        if(request.getChatName().equals("")){
            throw new BadInputException();
        }
        List<String> userNames = request.getUserNames();
        if(userNames.size() < 2){
            throw new NotEnoughUsersException();
        }

        List<User> users = new LinkedList<>();
        for(String name:userNames){
            User user = userRepository.findByUserName(name);
            if(user==null){
                throw new UserDoesNotExistsException();
            }
            users.add(user);
        }

        Chat chat = new Chat(null , request.getChatName(),  users , new LinkedList<>());
        chatRepository.save(chat);
        return true;
    }

    public List<MessageDTO> getMessages(Long chatId) throws Exception{
        Chat chat = chatRepository.findByChatId(chatId);
        if(chat == null){
            throw new ChatDoesNotExistsException();
        }else{
            List<Message> messages = chat.getChatMessages();
            List<MessageDTO> dtoMessages = new LinkedList<>();
            for(Message m:messages){
                MessageDTO toAdd = MessageMapper.messageMapToMessageDTO(m);
                dtoMessages.add(toAdd);
            }
            return dtoMessages;
        }
    }

}
