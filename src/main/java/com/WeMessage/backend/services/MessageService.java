package com.WeMessage.backend.services;

import com.WeMessage.backend.DTOs.MessageDTO;
import com.WeMessage.backend.entities.Chat;
import com.WeMessage.backend.entities.Message;
import com.WeMessage.backend.entities.User;
import com.WeMessage.backend.exceptions.BadInputException;
import com.WeMessage.backend.exceptions.ChatDoesNotExistsException;
import com.WeMessage.backend.exceptions.UserDoesNotExistsException;
import com.WeMessage.backend.repositories.ChatRepository;
import com.WeMessage.backend.repositories.MessageRepository;
import com.WeMessage.backend.repositories.UserRepository;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class MessageService {

    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ChatRepository chatRepository;

    public Long addMessage(MessageDTO messageToAdd) throws Exception{
        if(messageToAdd.getBody().equals("")){
            throw new BadInputException();
        }
        User user = userRepository.findByUserName(messageToAdd.getSenderUserName());
        if(user == null){
            throw new UserDoesNotExistsException();
        }

        Chat chat = chatRepository.findByChatId(messageToAdd.getChatId());
        if(chat == null){
            throw new ChatDoesNotExistsException();
        }

        Message message = new Message(null, messageToAdd.getDate() , messageToAdd.getBody() , user , chat);
        Message newMessage = messageRepository.save(message);
        return newMessage.getId();
    }

}
