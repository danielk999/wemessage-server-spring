FROM maven:3.8.3-jdk-11-slim
WORKDIR /workspace
COPY pom.xml ./
COPY src ./src
EXPOSE 8080
ENTRYPOINT ["mvn" , "spring-boot:run"]